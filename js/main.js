 function getRelatedContent(el){
    return $($(el).attr('href'));
}
// Get link by section or article id
function getRelatedNavigation(el){
    return $('nav a[href=\\#'+$(el).attr('id')+']');
}

$(function () {
    var logo = $('.logo-content'),
        body = $('body'),
        navPills =  $('#nav'),
        tab = navPills.find('.nav-link'),
        rightContent = $('.split.right'),
        slideNext = $('.slide-next'),
        slideDown = $('.slide-down a'),
        buttonToMenu = $('.btn-to-menu'),
        toggleContent = $('.toggled-content'),
        sections = $('.section'),
        nav = $('nav');

    logo.find('img').add(slideNext).on('click', function() {
        logo.toggleClass('minified');
        rightContent.addClass('about-tab')
        $('#centerContent').toggleClass('hidden');
        toggleContent.toggleClass('visible');
        $('.split.left').toggleClass('centered');
    })
    buttonToMenu.on('click', function () {
        logo.toggleClass('minified');
        rightContent.addClass('menu-tab')
        $('#centerContent').toggleClass('hidden');
        toggleContent.toggleClass('visible');
        $('.split.left').toggleClass('centered');
        logo.find('img').width(100)
        $('html,body').animate({scrollTop:getRelatedContent(this).offset().top - 79})
        //body.toggleClass('menu-active');
    })

    tab.each(function() {
        $(this).on('click', function () {
            tab.removeClass('active');
            $(this).addClass('active');
            var tabName = $(this).attr('id');
            $('.section').each(function () {
                if ( $(this).attr('data-conect') === tabName) {
                    $(this).addClass('show active');
                } else {
                    $(this).removeClass('show active');
                }
                body.removeClass('menu-active');

            })

            rightContent.removeClass().addClass('split right ' + tabName);
        })
    })

    slideDown.on('click', function () {
        tab.removeClass('active');
        $('#menu-tab').addClass('active');
        $('#pills-menu').addClass('show active');
        $('#pills-about').removeClass('show active');
        logo.find('img').width(100)
        rightContent.removeClass().addClass('split right menu-tab');
    })

    //menu toggler
    $(window).on('scroll', function () {
        var cur_pos = $(this).scrollTop(),
            sticky = $('#nav'),
            scroll = $(window).scrollTop();

        if( (cur_pos<1) && (!buttonToMenu.is(":visible")) ){
            logo.toggleClass('minified');
            rightContent.removeClass('about-tab').addClass('home-page')
            $('#centerContent').toggleClass('hidden');
            toggleContent.toggleClass('visible');
            $('.split.left').toggleClass('centered');
        }

        sections.each(function() {
            var top = $(this).offset().top - 80,
                bottom = top + $(this).outerHeight();

            if (cur_pos >= top && cur_pos <= bottom) {
                nav.find('a').removeClass('active');
                sections.removeClass('active');

                $(this).addClass('active');
                nav.find('a[href=\\#'+$(this).attr('id')+']').addClass('active');
                
                var tabName = $(this).attr('data-conect').substring(1);
                rightContent.removeClass().addClass('split right ' + tabName);
            }
        });

        if (scroll >= 70) {
            sticky.addClass('sticky');
            logo.addClass('sticky');
        }else{
            sticky.removeClass('sticky');
            logo.removeClass('sticky');
        }
    });

    //scrolling 
    $(window).on ('mousewheel', function (e) {
    //$(window).mousewheel(function(e,delta) {
        var delta = e.originalEvent.deltaY;
         if (delta > 0) {
            if(buttonToMenu.is(":visible")){
                logo.toggleClass('minified');
                rightContent.addClass('about-tab')
                $('#centerContent').toggleClass('hidden');
                toggleContent.toggleClass('visible');
                $('.split.left').toggleClass('centered');
            } 
         }
        return false;
    });



    $(document).on('click',function(){
        $('.collapse').collapse('hide');
        $('.collapsed').removeClass('change');
    })

    $('.reservation-btn').on('click', function(e) {
        $('.nav-link').removeClass('active');
        $('#reservation-tab').addClass('active');
        e.preventDefault();
        $('html,body').animate({scrollTop:getRelatedContent(this).offset().top - 79})
    })

    $('nav a').on('click', function(e){
        e.preventDefault();
        $('html,body').animate({scrollTop:getRelatedContent(this).offset().top - 79})
    });

    $('.section').each(function(){
        getRelatedNavigation(this).text();
    })
});